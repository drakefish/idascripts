# Name: NW_DumpServerCMDWrappers.py
# Author: DrakeFish
# Description: Dumps the autogen'd servercmdwrappers and names their functions.
# Status: Complete (unless you want to add args parsing)

TEXT_TO_FIND = "c:\\src\\crossroads\\common\\autogen\\gameserverlib_autogen_servercmdwrappers.c"

text = FindBinary(0, SEARCH_DOWN|SEARCH_CASE, " ".join("{0:x}".format(ord(c)) for c in TEXT_TO_FIND))

with open("autogen_servercmdwrappers.txt", "w") as file:
	count = 0
	lastfunc = 0
	textref = DfirstB(text)
	while textref != BADADDR:
		func = PrevFunction(textref)
		if func != lastfunc:
			# get the second ref of the function
			textref = DnextB(text, textref)
			instr = NextNotTail(textref)
			if GetMnem(instr) == "push":
				str = GetString(GetOperandValue(instr, 0)).strip()
				if str is not None and str != "":
					file.write("{0:8X} {1}\n".format(func, str))
					MakeName(func, "cmdwrapper_%s"%str)
					#print("{0:8X} {1}".format(func, str))
					count += 1
		
		lastfunc = func
		textref = DnextB(text, textref)
	
print("done. count: %d"%count)
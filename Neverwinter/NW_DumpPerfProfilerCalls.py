# Name: NW_DumpPerfProfilerCalls
# Author: DrakeFish
# Description: Dumps the calls to the game's performance profiler and the string associated with them.
# Status: Usable

# note: make a list and sort them by name maybe?

BIN_TO_SEARCH = "A1 ? ? ? ? 56 8B F2 89 4D FC 85 C0 74 0A 8B 40 1C FF D0 5E 8B E5 5D C3"
COUNT = 0;

print "NW DumpPerfProfilerCalls script started..."

func = PrevFunction(FindBinary(0, SEARCH_DOWN | SEARCH_CASE, BIN_TO_SEARCH))
if func != BADADDR:
	with open("profilercallsdump.txt", "w") as file:
		xref = RfirstB(func)
		while xref != BADADDR:
			instr = PrevHead(xref, 0)
			for i in range(0, 5): # max prev instructions count 
				if GetMnem(instr) == "mov":
					if GetOpType(instr, 0) == o_reg and GetOpnd(instr, 0) == "ecx":
						str = GetString(Dword(LocByName(GetOpnd(instr, 1))))
						if str != "":
							file.write("%8X"%xref+" %s\n"%str);
							COUNT+=1
				instr = PrevHead(instr, 0)
			xref = RnextB(func, xref)
			
print "Done (count:%d)"%COUNT
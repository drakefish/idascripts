# Name: NW_FindFunctions2.py
# Author: DrakeFish
# Description: Dumps a few functions/variables from some scripting system.
# Status: Usable

TEXT_TO_FIND = "Public access level 0 comand takes a tp block: %s"

COUNT = 0

print "NW Functions and Variables Dumper v0.1 starting..."

text = FindBinary(0, SEARCH_DOWN|SEARCH_CASE, " ".join("{0:x}".format(ord(c)) for c in TEXT_TO_FIND))

with open("funcsvarsdump.txt", "w") as file:
	textref = DfirstB(text)
	while textref != BADADDR:
		mainfunc = PrevFunction(textref)
		funcref = RfirstB(mainfunc)
		while funcref != BADADDR:
			instr = PrevHead(PrevHead(funcref, 0), 0) # we could output the other ptr if needed
			if GetMnem(instr) == "push":
				info_base = GetOperandValue(instr, 0)
				info_type = Dword(info_base)
				info_name = GetString(Dword(info_base+4))
				info_filename = GetString(Dword(info_base+8))
				info_type2 = Dword(info_base+0x19C)#0xC)
				info_lib = GetString(Dword(info_base+0x14))
				info_comment = GetString(Dword(info_base+0x1A0))
				info_ptr = Dword(info_base+0x1A4)
				# todo: check for arg names
				cmtstr = "Not a function."
				if info_ptr != 0:
					if LocByName("autogen_%s"%info_name) == BADADDR:
						MakeName(info_ptr, "autogen_%s"%info_name)
					cmtstr = "args: "
					arg = info_base+0x1C
					while Dword(arg) != 0:
						argname = GetString(Dword(arg))
						argtypename = GetString(Dword(Dword(arg+8)))
						if (argtypename is not None):
							cmtstr += argtypename + " "
						if (argname is None):
							break
						cmtstr += argname + ", "
						arg = arg+0x20
					SetFunctionCmt(info_ptr, cmtstr, 1)
				str = "\nbase:\t{0:X}\nname:\t{1}\nfile:\t{2}\nlib:\t{3}\ncomm:\t{4}\nptr:\t{5:X}\n{6}\n\n".format(info_base, info_name, info_filename, info_lib, info_comment, info_ptr, cmtstr)
				file.write(str)
				COUNT += 1
			funcref = RnextB(mainfunc, funcref)
		textref = DnextB(text, textref)

print "done (count: %s)"%COUNT
# Name: NW_FindFunctions.py
# Author: DrakeFish
# Description: Dumps some script functions and their arguments (dumps the type name for defined struct types)
# Status: Usable (more funcs arg functionalities could be added)

BIN_TO_SEARCH = "? ? ? ? 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ? ? ? ? 02 00 00 00  00 00 00 00  00 00 00 00  00 00 00 00  ?? ?? ?? ?? 03 00 00 00"
COUNT = 0

print "NW ScriptsDump v0.1 starting..."

start = FindBinary(0, SEARCH_DOWN | SEARCH_CASE, BIN_TO_SEARCH)

if (start != BADADDR):
	with open("scriptdump.txt", "w") as file:
		for i in range(0, 0x38):
			ea = Dword(start+i*0x14) # ea is the script "section" name
			str = GetString(ea)
			file.write("\n"+str+"\n")
			# we now need to iterate though xrefs to the name and find valid ones
			xref = DfirstB(ea)
			while xref != BADADDR:
				if SegName(xref) == ".data":
					func = Dword(xref-0x178)
					name = GetString(Dword(xref-0x170))
					if GetFunctionName(func) != "" and name is not None and name != "":
						argsstr = ""
						arg = xref - 0x16C
						while Dword(arg+4) != 0:
							argname = GetString(Dword(arg+4))
							argtype = Dword(arg)
							argprefix = ""
							if Dword(arg+0xC) != 0:
								argprefix = GetString(Dword(Dword(arg+0xC))) + "* "
								
							argsstr += argprefix + argname + ", "
							
							arg += 0x1C
						file.write("\t%8X"%func+" "+name+" ("+argsstr+")\n")
						MakeName(func, "script_"+str+"_"+name)
						COUNT+=1
				xref = DnextB(ea, xref)
			
print "done (count: %s)"%COUNT
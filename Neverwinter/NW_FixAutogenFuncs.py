# Name: NW_FixAutoGenFuncs.py
# Author: DrakeFish
# Description: Fixes and dumps the automatically generated functions
# Status: Usable

TEXT_TO_FIND = "_AutoGen_1.c"
result = 0

with open("autogenfuncs.txt", "w") as file:
	while result != BADADDR:
		result = FindBinary(result+1, SEARCH_DOWN|SEARCH_CASE, " ".join("{0:x}".format(ord(c)) for c in TEXT_TO_FIND))
		text = PrevNotTail(result)
		string = GetString(text)

		print(string)
		
		textref = DfirstB(text)
		while textref != BADADDR:
			if GetFunctionName(textref) == "":
				
				nav = PrevNotTail(textref)
				while GetMnem(nav) != "":
					nav = PrevNotTail(nav)
				start = NextNotTail(nav)
				
				nav = NextNotTail(textref)
				while GetMnem(nav) != "":
					nav = NextNotTail(nav)
				end = PrevNotTail(nav)
				
				MakeFunction(start, end)
				#print("func: {0:X} {1:X}".format(start, end))
				#file.write("{0:8X} {1}\n".format(start, string))
			
			textref = DnextB(text, textref)
	
print("done fixing.")
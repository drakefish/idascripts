#
# Author: DrakeFish
# Date: May 7, 2013
# Description: Dumps the available types (enums and structs) to a file and adds them to the IDA database.
# Status: Doesn't work (unfinished yet)

OPTION_ENUMS_FILE = "NW_DUMP_ENUMS.txt" # keep empty to disable
OPTION_STRUCTS_FILE = "NW_DUMP_STRUCTS.txt" # keep empty to disable
OPTION_CREATE_IDA_ENUMS = 1
OPTION_CREATE_IDA_STRUCTS = 1

def main():
	print("Neverwinter Online Types Dumper")
	enumsDump()
	structsDump()
	print("done.")
	
def enumsDump():
	print("dumping enums...")
	
	ea = PrevFunction(DfirstB(FindBinary(0, SEARCH_DOWN|SEARCH_CASE, " 44 75 70 6C 69 63 61 74 65 20 65 6E 75 6D 73 20 6E 61 6D 65 64 20 25 73")))
	if ea != BADADDR:
		xref = RfirstB(ea)
		print("xref: %X"%xref)
		while xref != BADADDR:
			push = PrevHead(xref, 0)
			if GetMnem(push) == "push": # todo: loop to get back to last push
				enumdef = GetOperandValue(push, 0)
				push = PrevHead(xref, 0)
				if GetMnem(push) == "push": # todo: loop to get back too (we could make a func)
					str = GetOperandValue(push, 0)
					print(GetString(str)) # todo: fix it, this line shows "None"
				else:
					print("%X8 invalid found (reason 2)"%xref)
			else:
				print("%X8 invalid found (reason 1)"%xref)
			xref = RnextB(ea, xref)
		
# this should basically create all structs and then go through them against to add ref's to eachothers
def structsDump():
	print("dumping structs...")
	

main()




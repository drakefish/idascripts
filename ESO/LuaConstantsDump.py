# Title: LuaConstantsDump.py
# Author: DrakeFish
# Description: Dumps the lua constant values and the events to two different files.
# Status: Complete

PATTERN_ADDCONSTANTFUNC = "55 8B EC 8B 51 04 DB 45 0C 8B 42 24 C7 00 03 00 00 00"
COUNT = 0

print "ESO Constants Dump started."

constant_count = 0
mainfunc = FindBinary(0, SEARCH_DOWN, PATTERN_ADDCONSTANTFUNC)

if mainfunc != BADADDR:
	with open("constantsdump.txt", "w") as file:
		with open("luaeventsdump.txt", "w") as event_file:
			lasteventfunc = 0
			call = RfirstB(mainfunc)
			while call != BADADDR:
				push = PrevHead(PrevHead(call, 0), 0)
				name = GetString(GetOperandValue(push, 0))
				if name.startswith("EVENT_"):
					# handle as lua event
					if PrevFunction(call) != lasteventfunc:
						event_file.write("\n")
						lasteventfunc = PrevFunction(call)
						
					op = PrevHead(PrevHead(push, 0), 0)
					value = 0
					if GetMnem(op) == "or":
						value = GetOperandValue(op, 1)
					event_file.write("%s = 0x%X\n"%(name, value))
					
				else:
					# handle as a regular constant value
					value = GetOperandValue(PrevHead(push, 0), 0)
					file.write("%s = 0x%X\n"%(name, value))
				
				call = RnextB(mainfunc, call)
			
print "done"
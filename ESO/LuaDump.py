# Name: LuaDump
# Author: DrakeFish
# Description: Dumps and names all the native lua functions in the game
# Status: Complete

# current "bug": there are 2 duplicated functions in two different "contexts". 
#  I can't really do anything about it for naming.

# dump basic functions
# note: patch 0.26.4.543804 offset for this func: 00785790
PATTERN_ADDLUAFUNC = "55 8B EC 8B 45 10 56 57 8B 7D 08 6A 00"
# note: patch 0.26.4.543804 offset for this func: 0074D930
PATTERN_ADDLUACLASS = "55 8B EC 8B 45 08 56 8B F1 8B 4E 04 50 51 C7 86 38 0A 00 00 00 00 00 00"
PATTERN_ARGFAILSTR = " ".join("{0:x}".format(ord(c)) for c in "Checking type on argument %s failed in %s")

argfailedstr = FindBinary(0, SEARCH_DOWN, PATTERN_ARGFAILSTR)
def GetArgsForLuaFunc(ea):
	"returns an array of arguments for this lua function"
	result = []
	funcend = FindFuncEnd(ea)
	if funcend == BADADDR:
		return []
	
	head = NextHead(ea, BADADDR)
	while head < funcend:
		if GetMnem(head) == "push":
			if (GetOperandValue(head, 0) == argfailedstr):
				# this is an argument.
				argname = GetString(GetOperandValue(PrevHead(head, 0), 0))
				result.append(argname)
		head = NextHead(head, BADADDR)
	
	return result
	
# basic functions dumper
basic_funcs_count = 0
addfunc = FindBinary(0, SEARCH_DOWN, PATTERN_ADDLUAFUNC)
if addfunc != BADADDR:
	with open("luadump.txt", "w") as file:
		lastfunc = 0 # this is the last function containing the call
		call = RfirstB(addfunc)
		while (call != BADADDR):
			if PrevFunction(call) != lastfunc:
				file.write("\n")
				lastfunc = PrevFunction(call)
			# get the pushed values
			push = PrevHead(PrevHead(call, 0), 0)
			name = GetString(GetOperandValue(push, 0))
			push = PrevHead(push, 0)
			func = GetOperandValue(push, 0)
			push = PrevHead(push, 0)
			flags = GetOperandValue(push, 0)
			
			endstr = ""
			if flags == 2:
				endstr = "*"
			
			# args
			args = GetArgsForLuaFunc(func)
			argsstr = ""
			if args.count > 0:
				for arg in args:
					argsstr += arg + ", "
				argsstr = argsstr.rstrip(", ") # remove additional separator
			
			file.write("%8X %s(%s) %s\n"%(func,name,argsstr,endstr))
			MakeName(func, "Script_"+name)
			basic_funcs_count += 1
			
			call = RnextB(addfunc, call)
		
		file.write("\ncount: %d"%basic_funcs_count)

# classes metadata dumper
classes_count = 0
addfunc = FindBinary(0, SEARCH_DOWN, PATTERN_ADDLUACLASS)
if addfunc != BADADDR:
	with open("luaclassdump.txt", "w") as file:
		call = RfirstB(addfunc)
		while call != BADADDR:
			classname = GetString(GetOperandValue(PrevHead(call, 0), 0)).split("Lua_metatable")[0]
			classoffs = GetOperandValue(PrevHead(PrevHead(call, 0), 0), 0)
			
			file.write("\n%s:\n"%classname)
			classes_count += 1
			
			curroffs = classoffs
			while True:
				funcname = GetString(Dword(curroffs))
				funcoffs = Dword(curroffs+4)
				
				args = GetArgsForLuaFunc(funcoffs)
				argsstr = ""
				if args.count > 0:
					for arg in args:
						argsstr += arg + ", "
					argsstr = argsstr.rstrip(", ") # remove additional separator
				
				file.write("\t%8X %s(%s)\n"%(funcoffs, funcname, argsstr))
				MakeName(funcoffs, "Script_%s_%s"%(classname, funcname))
				
				curroffs += 8
				if Dword(curroffs) == 0:
					break
			
			call = RnextB(addfunc, call)

print "done. funcs count: %d  classes count: %d"%(basic_funcs_count, classes_count)

	


# Name: DBTableStructDump.py
# Author: DrakeFish
# Description: Dumps the structures for each .tbl
# Status: WIP

# function as of 0.5.3.6160: sub_50EAA0

# The GetTableStructures() function returns a dictionary with the following "structure":
#  returnValue: { name:structure, .. }
#  structure: { name:field, .. }
#  field: name, offset, size, type ** This will probably change

import xml.etree.ElementTree as ET

PATTERN_LOADTBL = "55 8B EC 81 EC ? ? ? ? A1 ? ? ? ? 33 C5 89 45 FC 53 8B 5D 0C 56 57 8B 3D"

func_loadtbl = PrevFunction(FindBinary(0, SEARCH_DOWN, PATTERN_LOADTBL))


def GetLastPush(ea):
	for x in xrange(10):
		ea = PrevHead(ea, 0)
		if GetMnem(ea) == "push":
			return ea
	return BADADDR

class FieldEntry():
	def __init__(self, fieldBase):
		self.name = GetString(Dword(fieldBase), -1, ASCSTR_UNICODE)
		self.data = []
		for i in xrange(6):
			self.data.append(Dword(fieldBase+4+i*4))
	
class TableEntry():
	def __init__(self, name, defBase):
		self.name = name
		self.size = Dword(defBase+4)
		self.count = Dword(defBase+8)
		fieldBase = Dword(defBase+0xC)
		self.fields = []
		for x in xrange(self.count):
			field = FieldEntry(fieldBase)
			self.fields.append(field)
			fieldBase+=0x1C
	
def GetTableStructures():
	tables = []
	call = RfirstB(func_loadtbl)
	while call != BADADDR:
		namePush = GetLastPush(call)
		if namePush != BADADDR or GetOpType(namePush, 0) != o_imm:
			defPush = GetLastPush(namePush)
			if defPush != BADADDR or GetOpType(defPush, 0) != o_imm:
				name = GetString(GetOperandValue(namePush, 0), -1, ASCSTR_UNICODE)
				defBase = GetOperandValue(defPush, 0)
				table = TableEntry(name, defBase)
				#tables[name] = [ ] # init table entry
				tables.append(table)
			else:
				print("Failed to get def ptr push")
		else:
			print("Failed to get name ptr push")
		call = RnextB(func_loadtbl, call)
	return tables
		
with open("tblStructDump.txt", "w") as file:
	tables = GetTableStructures()
	#for table in tables.keys():
	#	file.write("\n%s\n"%(table))
	#	for field in tables[table].keys:
	#		file.write("%s:%s\n"%(field.name, field.data))
	for table in tables:
		file.write("\n%s\n"%(table.name))
		for field in table.fields:
			file.write("%s:%s\n"%(field.data, field.name))
			
	# writing xml test
	root = ET.Element("descriptors")
	root.set("c", str(len(tables)))
	for table in tables:
		tableElement = ET.SubElement(root, "t")
		tableElement.set("n", table.name)
		tableElement.set("s", str(table.size))
		tableElement.set("c", str(table.count))
		for field in table.fields:
			fieldElement = ET.SubElement(tableElement, "f")
			fieldElement.set("n", field.name)
			for x in xrange(6):
				fieldElement.set("d%d"%x, str(field.data[x]))
		tree = ET.ElementTree(root)
	tree.write("TblDescriptors.xml")
	
	# write to .CS file
	with open("DataTables.cs", "w") as file:
		file.write("using System;\n\nnamespace FishyEmu.DataTables\n{")
		for table in tables:
			file.write("\n\t[DataTableDescription(\"%s\")]\n"%table.name.replace("\\", "/"))
			className = table.name.split('\\')[1].split('.')[0]
			file.write("\tpublic class Tbl%s : DataTable<Tbl%s>\n\t{"%(className, className))
			for field in table.fields:
				if field.name == "id":
					continue # don't add the id, it's in the abstract class.
				if field.data[0] == 130:
					file.write("\n\t\t[DataTblField(0x%X, %d)] "%(field.data[1], field.data[2]))
					file.write("public readonly String %s;"%field.name)
				else:
					if field.data[0] == 3:
						type = "UInt32"
					elif field.data[0] == 4:
						type = "Single"
					elif field.data[0] == 11:
						type = "Boolean"
					elif field.data[0] == 20:
						type = "UInt64"
					else:
						print("invalid type found")
					file.write("\n\t\t[DataTblField(0x%X)] "%field.data[1])
					file.write("public readonly %s %s;"%(type, field.name))
			file.write("\n\t}\n");
		
	print("Done.")
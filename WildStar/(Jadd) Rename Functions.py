FunctionPatterns = [
	#============================================
	# GENERAL FUNCTIONS
	#============================================
	("", [
		("Buffer_GetEnd", "8B D1 8B 42 10 85 C0"),
		("CallContact", "55 8B EC 83 EC 10 A1 ? ? ? ? 53 8B 40 3C"),
		("CreateNetworkBuffer_WithSize", "55 8B EC 8B 45 08 56 8B F1 6A 00 C7 06 ? ? ? ? C7 46"),
		("DataProtector_Create", "55 8B EC 83 EC 14 53 56 8D 71 08"),
		("DataProtector_InitPacketCrypt", "55 8B EC 83 E4 F8 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? 53 8B C1"),
		("GetLocalizedTextString", "55 8B EC A1 ? ? ? ? 8B 0D ? ? ? ? 8B 00"),
		("InitializeCharacterScreen", "55 8B EC 83 E4 F8 83 EC 08 56 8B F1 57 83 7E 4C 00"),
		("InitializeLoginScreen", "55 8B EC 83 E4 F8 51 A1 ? ? ? ? 56 6A 00"),
		("InitializeRealmConnection", "55 8B EC 83 E4 F0 83 EC 58 A1 ? ? ? ? 33 C4 89 44 24 54 56 0F 57 C0"),
		("Network_GetMessageData_0x0C", "8D 46 08 50 FF 75 08 8B 43 10 FF D0 83 C4 08 85 C0 75 10 8B 76 04 85 F6 75 E2 5F 5E 33 C0 5B 5D C2 04 00 5F 8D 46 0C"),
		("Network_GetMessageDef", "8D 46 08 50 FF 75 08 8B 43 10 FF D0 83 C4 08 85 C0 75 10 8B 76 04 85 F6 75 E2 5F 5E 33 C0 5B 5D C2 04 00 5F 8D 46 10"),
		("Network_GetMessageData_0x14", "8D 46 08 50 FF 75 08 8B 43 10 FF D0 83 C4 08 85 C0 75 10 8B 76 04 85 F6 75 E2 5F 5E 33 C0 5B 5D C2 04 00 5F 8D 46 14"),
		("Network_GetMessageData_0x18", "8D 46 08 50 FF 75 08 8B 43 10 FF D0 83 C4 08 85 C0 75 10 8B 76 04 85 F6 75 E2 5F 5E 33 C0 5B 5D C2 04 00 5F 8D 46 18"),
		("Network_GetMessageData_0x1C", "8D 46 08 50 FF 75 08 8B 43 10 FF D0 83 C4 08 85 C0 75 10 8B 76 04 85 F6 75 E2 5F 5E 33 C0 5B 5D C2 04 00 5F 8D 46 1C"),
		("Network_GetMessageData_0x20", "8D 46 08 50 FF 75 08 8B 43 10 FF D0 83 C4 08 85 C0 75 10 8B 76 04 85 F6 75 E2 5F 5E 33 C0 5B 5D C2 04 00 5F 8D 46 20"),
		("Network_GetMessageData_0x30", "8D 46 08 50 FF 75 08 8B 43 10 FF D0 83 C4 08 85 C0 75 10 8B 76 04 85 F6 75 E2 5F 5E 33 C0 5B 5D C2 04 00 5F 8D 46 30"),
		("Network_GetMessageData_0x3C", "8D 46 08 50 FF 75 08 8B 43 10 FF D0 83 C4 08 85 C0 75 10 8B 76 04 85 F6 75 E2 5F 5E 33 C0 5B 5D C2 04 00 5F 8D 46 3C"),
		("Network_GetMessageData_0x110", "8D 46 08 50 FF 75 08 8B 43 10 FF D0 83 C4 08 85 C0 75 10 8B 76 04 85 F6 75 E2 5F 5E 33 C0 5B 5D C2 04 00 5F 8D 86 10 01 00 00"),
		("Network_GetMessageData_0x254", "8D 46 08 50 FF 75 08 8B 43 10 FF D0 83 C4 08 85 C0 75 10 8B 76 04 85 F6 75 E2 5F 5E 33 C0 5B 5D C2 04 00 5F 8D 86 54 02 00 00"),
		("Network_GetMessageStringForId", "A1 ? ? ? ? 56 8B F1 85 C0 74 46"),
		("Network_ReceiveAndUnwrapMessage", "55 8B EC 83 E4 F8 81 EC ? ? ? ? 53 56 57 8B 7D 10"),
		("Network_SendPackedMessage", "55 8B EC 56 FF 75 10 8B F1 8B 0D"),
		("Network_UnkAlgorithm", "01 7E 20 53 57 11 5E 24 53 57 E8"),
		("PacketCrypt_Decrypt", "55 8B EC 83 E4 F8 83 EC 24 8B 81"),
		("PacketCrypt_Encrypt", "55 8B EC 83 E4 F8 83 EC 1C 8B 81"),
		("PacketCrypt_ctor", "55 8B EC 83 E4 F8 51 53 8B D9 56 8B 75 08"),
		("Packet_CreateMessageHandler", "55 8B EC 51 8B 45 0C 56 8B F1 0F 57 C0"),
		("Packet_GetMaxPossibleSize", "83 F9 03 73 06"),
		("Packet_PackAndSend", "55 8B EC 83 E4 F8 83 EC ? 53 56 57 8B FA 8B F1 8B 47 08"),
		("Packet_ReadInteger", "55 8B EC 51 53 8B D9 56 8B 43 04 57"),
		("Packet_ReadUUID", "55 8B EC 83 E4 F8 83 EC 0C 8D 44 24 04 53 56 57 6A 01 50 8B F1"),
		("Packet_RegisterConstantMessages", "55 8B EC 56 8B 75 08 68 ? ? ? ? 8B 06"),
		("Packet_WriteInteger", "51 8B 41 04 56 85 C0 74 ? 8D 70 10 8B 40 0C 2B 06 8D 51 08 C1 E0 03"),
		("Packet_WriteInteger16", "0F ? ? 08 99 6A ?? 52 8D 51 08"),
		("Packet_WriteInteger64", "72 19 6A 40 FF 75 0C 8B CE"),
		("Packet_WriteObject", "55 8B EC 8B D1 56 8B 4A 04"),
		("Packet_WriteRawData", "56 2B C3 8B F1 33 C9 57 89 45 0C A8 01 74"),
		("Packet_WriteSingle", "F3 0F 11 4C 24 04 85 C0 74"),
		("Packet_WriteWString", "75 ?? 8D 04 73 50 53 8B CF E8"),
		("RegisterAddonSlashCommands", "55 8B EC 83 EC ? 53 56 57 8B 3D ? ? ? ? 89 ? ? 8D 4F 08"),
		("RegisterSlashCommand", "55 8B EC 83 EC 18 53 56 8B 75 08 57 8B F9 85 F6 74 7D"),
		("ShowGenericSplashError", "06 00 00 00 85 C9 74 07 33 D2 E8 ? ? ? ? 8D 4C 24 08"),
	]),
	
	#============================================
	# APOLLO FUNCTIONS
	#============================================
	("Apollo", [
		("ApolloMgr__HandleLuaEventById", "89 7C 24 ? 85 FF 74 07 8B CF E8 ? ? ? ? 8B 45 0C"),
		("ApolloMgr__HandleLuaEventByName", "89 7C 24 ? 85 FF 74 07 8B CF E8 ? ? ? ? 8B 75 08"),
		("ApolloMgr__HandleLuaEventFromLua", "89 7C 24 ? 85 FF 74 07 8B CF E8 ? ? ? ? 8B 5D 08"),
		("LuaLander__CallLua_1", "55 8B EC 83 E4 F8 83 EC 5C A1 ? ? ? ? 53 56 57 8B F9 89 7C 24 10 A8 01"),
		("LuaLander__CallLua_2", "55 8B EC 83 E4 F8 83 EC 5C A1 ? ? ? ? 53 56 57 8B F9 89 7C 24 14"),
		("LuaLander__CallLua_3", "55 8B EC 83 E4 F8 83 EC 5C A1 ? ? ? ? 53 56 8B D9"),
		("LuaLander__LuaLineHook", "55 8B EC 83 E4 F8 A1 ? ? ? ? 83 EC 2C 53 56 57 A8 01"),
		("LuaLander__LuaPCall", "55 8B EC 83 E4 F8 83 EC 08 56 8B F2"),
		("LuaLander__ProtectedCall", "55 8B EC 83 E4 C0 83 EC 74 A1 ? ? ? ? 33 C4 89 44 24 70 A1"),
		("LuaLander__PushString", "55 8B EC 8B 45 08 53 8B 59 08"),
		("Window__HandleLuaEventArgs", "55 8B EC 83 E4 F8 83 EC 24 A1 ? ? ? ? 53 56 8B F1 57 89 74 24 10"),
	]),
	
	#============================================
	# CCAMERA FUNCTIONS
	#============================================
	("CCamera", [
		("ctor", "C7 01 ? ? ? ? C7 41 ? ? ? ? ? A1"),
	]),
	
	#============================================
	# CEVENTQUEUE FUNCTIONS
	#============================================
	("CEventQueue", [
		("Poll", "55 8B EC 83 E4 F8 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? 53 8B 1D ? ? ? ? 56 57"),
		("Register", "55 8B EC 83 E4 F8 83 EC 2C 53 56 8B F1 57 81 3E"),
		("Remove", "55 8B EC 83 EC 14 56 8B F1 81 3E"),
	]),
	
	#============================================
	# CGAME FUNCTIONS
	#============================================
	("CGame", [
		("AssistTarget", "55 8B EC A1 ? ? ? ? 83 EC 08 8B 80 ? ? ? ? 85 C0 74 ? 83 ? ? 00 00 00 02"),
		("CameraToggleAngle", "55 8B EC 83 E4 F0 83 EC 10 83 B9 ? ? ? ? ? 74 44"),
		("CameraZoomIn", "55 8B EC 51 56 8B F1 8B 46 3C"),
		("CameraZoomOut", "55 8B EC 83 EC 08 56 8B F1 57 8B 46 3C 85 C0 0F 84 ? ? ? ? 8B 40 7C 8B 0D ? ? ? ? 89 45 FC"),
		("CastActionBarAbility", "74 ? 3C 04 73 ? 0F B6 C0 8D 04 40 C1 E0 04 03"),
		("CastActionBarSetIndex", "55 8B EC 81 EC ? ? ? ? 53 56 57 8B 7D 08 8B F1"),
		("CastActivateBind", "55 8B EC 83 E4 F8 81 EC ? ? ? ? 53 56 57 8B 7D 08 8B F1"),
		("CastActivateSpell", "55 8B EC 83 E4 F8 A1 ? ? ? ? 81 EC ? ? ? ? 53 56 57 8B F9"),
		("CastExplorerDig", "55 8B EC A1 ? ? ? ? 81 EC ? ? ? ? 53"),
		("CastFloatingActionBarAbility", "55 8B EC 83 EC 14 53 56 8B F1 57 83 BE"),
		("CastGadgetAbility", "55 8B EC 83 EC 18 57 8B F9 8B 0D"),
		("CastInnateAbility", "55 8B EC 83 EC 14 53 57 8B F9"),
		("CastObjectiveAbility", "55 8B EC 83 EC 30 8B D1"),
		("CastPetBarAbility", "55 8B EC 83 EC 14 53 56 57 8B F9 8B 0D ? ? ? ? E8 ? ? ? ? 8D 8F"),
		("CastShortcutSpell", "55 8B EC 81 EC ? ? ? ? 53 56 8B F1 57 8B 7E 3C"),
		("CastSpellBookIndex", "3D 00 01 00 00 72 ? B8 04 00 00 00"),
		("CompleteCrafting_1", "83 B8 ? ? ? ? ? 74 15 8B CE E8 ? ? ? ? B8 ? ? ? ? 5F 5E 5B 8B E5 5D C2 0C 00"),
		("CompleteCrafting_2", "83 B8 ? ? ? ? ? 74 15 8B CE E8 ? ? ? ? B8 ? ? ? ? 5F 5E 5B 8B E5 5D C2 08 00"),
		("Dash", "55 8B EC 83 E4 F8 81 EC ? ? ? ? 53 56 8B F1 57 83 7E 3C 00"),
		("HandleKeyBinding", "55 8B EC 83 E4 F8 51 8B 55 08 56 8B F1 8B 8E"),
		("InteractWithActorById", "55 8B EC 83 EC 10 53 56 57 8B F9 8B 77 3C"),
		("InternalCastBossToken", "55 8B EC 83 E4 F0 81 EC ? ? ? ? 56 57 8B F9 8B 87"),
		("InternalCastSpell", "83 ? ? 02 75 ? F7 80 ? ? ? ? 00 00 20 00"),
		("InternalCastSpellFromItem", "55 8B EC 81 EC ? ? ? ? 53 8B 5D 08 56 8B 73 40"),
		("IsMouseLooking", "56 8B F1 6A 76"),
		("Jump", "55 8B EC 53 56 8B F1 57 83 BE"),
		("MouseInteract", "56 8B F1 8B 46 3C 85 C0 74 40"),
		("MouseLook", "56 8B F1 8B 4E 3C 85 C9 74 10 E8 ? ? ? ? 85 C0 74 07 B8 ? ? ? ? 5E C3 83 BE"),
		("MouseTurn", "6A 20 8B 08 50 FF 51 ?  85 C0 74 09 83 BE ? ? ? ? ? 75 0D"),
		("MoveBackward", "74 0F 83 7D 0C 04 75 09 6A 00 6A 02"),
		("MoveForward", "74 0F 83 7D 0C 04 75 09 6A 00 6A 01"),
		("OnEscapeKey", "51 56 8B F1 57 8B 96"),
		("OnHostileInteractKey", "56 8B F1 57 8B 46 3C 85 C0 74 5B"),
		("OnInteractKey", "8B CE E8 ? ? ? ? 83 7D 08 00 74 0D FF B6 ? ? ? ? 8B CE E8"),
		("OnNetworkMessage", "55 8B EC 83 E4 F0 83 EC 68 56 57 8B F9 8B 8F ? ? ? ? 85 C9"),
		("OnSelectKey", "53 8B DC 83 EC 08 83 E4 F0 83 C4 04 55 8B 6B 04 89 6C 24 04 8B EC 83 EC 58 56 57 8B F9 8B 87 ? ? ? ? 89 87"),
		("OnSpellUnitCastSuccess", "55 8B EC 83 E4 F8 81 EC ? ? ? ? A1 ? ? ? ? 56"),
		("Roll", "55 8B EC 81 EC ? ? ? ? 53 56 8B F1 57 83 7E 3C 00"),
		("SendType11Packet", "C7 44 24 ? 0B 00 00 00 FF B0 ? ? ? ? E8 ? ? ? ? 5F 5E 8B E5 5D C2 08 00"),
		("SendType19Packet", "C7 44 24 ? 13 00 00 00 FF B0 ? ? ? ? E8 ? ? ? ? 5F 5E 8B E5 5D C2 08 00"),
		("SendType3Packet", "C7 44 24 ? 03 00 00 00 FF B0 ? ? ? ? E8 ? ? ? ? 5F 5E 8B E5 5D C2 08 00"),
		("SetTargetMarker", "55 8B EC 83 E4 F8 83 EC 08 56 8B 71 30"),
		("SpellCheck", "55 8B EC 81 EC ? ? ? ? 68"),
		("StartSprint", "55 8B EC 81 EC ? ? ? ? 53 56 57 8B F9 E8"),
		("StopSprint", "55 8B EC 83 EC 08 56 8B F1 57 8B 4E 3C"),
		("StrafeLeft", "74 0F 83 7D 0C 04 75 09 6A 00 6A 03"),
		("StrafeRight", "74 0F 83 7D 0C 04 75 09 6A 00 6A 04"),
		("StunBreakoutUp", "C2 ? 00 C7 86 ? ? ? ? 00 00 00 00 E8 ? ? ? ? 83 BE ? ? ? ? 00 74 07 80 8E ? ? ? ? 01"),
		("StunBreakoutDown", "C2 ? 00 C7 86 ? ? ? ? 00 00 00 00 E8 ? ? ? ? 83 BE ? ? ? ? 00 74 07 80 8E ? ? ? ? 02"),
		("StunBreakoutLeft", "C2 ? 00 C7 86 ? ? ? ? 00 00 00 00 E8 ? ? ? ? 83 BE ? ? ? ? 00 74 07 80 8E ? ? ? ? 04"),
		("StunBreakoutRight", "C2 ? 00 C7 86 ? ? ? ? 00 00 00 00 E8 ? ? ? ? 83 BE ? ? ? ? 00 74 07 80 8E ? ? ? ? 08"),
		("SupportStuck", "55 8B EC 81 EC ? ? ? ? 53 56 57 8B 3D"),
		("TargetActorById", "55 8B EC 83 E4 F8 83 EC 20 56 57 8B F9 83 BF ? ? ? ? ? 0F 84"),
		("TargetNearestActor", "85 F6 0F 84 ? ? ? ? 83 BE ? ? ? ? 00 0F 85 ? ? ? ? F6 46 ? 01"),
		("TargetNextActor", "55 8B EC 83 EC 08 56 8B F1 57 83 7E 3C 00"),
		("ToggleSheathWeapons", "8B 46 ? 33 C9 C7 45 FC 00 00 00 00 83 B8 ? ? ? ? 1F 8D 45 FC 0F 95 C1"),
		("TurnLeft", "74 11 83 7D 0C 04 75 0B 6A 00 6A 03"),
		("TurnRight", "74 11 83 7D 0C 04 75 0B 6A 00 6A 04"),
	]),
	
	#============================================
	# CGAMEAPP FUNCTIONS
	#============================================
	("CGameApp", [
		("ctor", "55 8B EC 83 E4 F8 51 56 8B F1 E8 ? ? ? ? C7 86"),
	]),
	
	#============================================
	# CGFXAPP FUNCTIONS
	#============================================
	("CGfxApp", [
		("ctor", "55 8B EC 83 EC 0C 83 3D ? ? ? ? ? 53 8B D9"),
	]),
	
	#============================================
	# CNETWORKMANAGER FUNCTIONS
	#============================================
	("CNetworkManager", [
		("AddUnkStruct", "55 8B EC 83 E4 F8 83 EC 0C 53 56 8B 75 08 8B C6"),
		("CreateNetworkBuffer", "55 8B EC 83 E4 F8 56 57 8B 7D 08 85 FF"),
		("GetMessageDef", "55 8B EC 8D 45 08 50 83 C1 44"),
		("PackMessage", "55 8B EC 83 E4 F8 8B 01 83 EC 1C"),
		("Poll", "55 8B EC 83 E4 F8 83 EC 0C 53 56 57 6A 00 8B F1"),
		("ProcessIncoming", "E9 ? ? ? ? 50 51 8B C4 6A 00 6A 0B"),
		("ProcessOutgoing", "76 ? FF ? ? 51 8B C4 6A 00 6A 0B"),
		("ReceiveAndUnpack", "55 8B EC 83 E4 F8 83 EC 4C 8D 44 24 24"),
		("SendMessageById_1", "55 8B EC 83 E4 F8 83 EC 24 53 56 8B 75 0C 57 56"),
		("SendMessageById_2", "55 8B EC 83 EC 08 8B 55 0C 53"),
		("SendPingReply", "55 8B EC 83 EC 08 56 0F B7 75 08"),
	]),
	
	#============================================
	# CPLAYER FUNCTIONS
	#============================================
	("CPlayer", [
		("SetStance", "E8 ? ? ? ? 8B 5D ? 3B D8 72 ? B8"),
	]),
	
	#============================================
	# CQUESTLOG FUNCTIONS
	#============================================
	("CQuestLog", [
		("ToggleQuestCallouts", "51 33 C0 39 41 ? 68 ? ? ? ? 0F 94 C0"),
	]),
	
	#============================================
	# CSPELLCOOLDOWN FUNCTIONS
	#============================================
	("CSpellCooldown", [
		("Remove", "55 8B EC 83 E4 F8 83 EC 1C 53 56 57 6A 21"),
	]),
	
	#============================================
	# CSPELLPREREQUISITE FUNCTIONS
	#============================================
	("CSpellPrerequisite", [
		("DurationEvent", "55 8B EC 83 E4 F8 83 EC 1C 53 56 57 6A 22"),
	]),
	
	#============================================
	# CWORLD FUNCTIONS
	#============================================
	("CWorld", [
		("Draw", "F6 C4 44 7A 0A C7 ? ? ? ? ? 00 00 80 3F 83"),
		("Update", "F7 D9 1B C9 83 E1 14 51 57 FF 50"),
	]),
	
	#============================================
	# CZOMBIEMANAGER FUNCTIONS
	#============================================
	("CZombieManager", [
		("Update", "69 D2 58 02 00 00 81 C2 A0 15 FF FF 03 D0 83 C0 0A"),
	]),
	
	#============================================
	# LOGINSCREENMGR FUNCTIONS
	#============================================
	("LoginScreenMgr", [
		("OnRealmListChanged", "55 8B EC 83 E4 F8 83 EC 64 8B C1"),
		("HandleMessage", "55 8B EC 83 E4 F8 83 79 4C 0A"),
	]),
	
	#============================================
	# MEMORY FUNCTIONS
	#============================================
	("Memory", [
		("Alloc", "8B ? ? 81 ? 00 01 00 00 77 ? 83 ? 02"),
	]),
	
	#============================================
	# NETWORKBUFFER FUNCTIONS
	#============================================
	("NetworkBuffer", [
		("GetCurrentSize", "55 8B EC 8B 4D 08 8B 41 18"),
		("GetEndPtr", "55 8B EC 8B 45 08 8B 40 14 5D"),
		("GetStartPtr", "55 8B EC 8B 45 08 8B 40 10 5D"),
		("Unk00", "55 8B EC 8B 4D 08 8B 41 04 40"),
		("Unk01", "55 8B EC 56 8B 75 08 FF 4E 04 8B 46 04 75 1B"),
	]),
	
	#============================================
	# REALMSELECTMGR FUNCTIONS
	#============================================
	("RealmSelectMgr", [
		("HandleMessage", "55 8B EC 83 E4 F8 8B 41 4C"),
		("OnSuccess", "55 8B EC 56 8B F1 83 7E 4C 00"),
		("OnWinMsg", "55 8B EC 51 8B C1 89 45 FC 8B 40 4C"),
		("ShowSplashError", "05 00 00 00 85 C9 74 07 33 D2 E8 ? ? ? ? 8D 4C 24 08"),
		("ctor", "A1 ? ? ? ? 8D 51 ? 89 02 89 0D ? ? ? ? 8B 02 85 C0 74 03 89 50 ? 8B C1 C3"),
	]),
	
	#============================================
	# UNKNETWORKSTRUCT FUNCTIONS
	#============================================
	("UnkNetworkStruct", [
		("ctor", "55 8B EC 8B 45 08 83 EC 08 56 57 8B F9 C7 07"),
		("dtor", "56 8B F1 C7 06 ? ? ? ? 8B 8E ? ? ? ? 85 C9 74 08"),
	]),
]

#================================================================================================
#================================================================================================
#================================================================================================
#================================================================================================

def Log(msg, newLine = True):
	file.write(msg)
	if newLine:
		file.write("\n")
		
def GetFuncStartAddress(address):
	return FirstFuncFchunk(address)

def GetFullFuncName(i, j):
	className = FunctionPatterns[i][0]
	funcName  = FunctionPatterns[i][1][j][0]
	
	if className == "":
		return funcName
	else:
		return "%s::%s" % (className, funcName)

with open("Offsets.cs", "w") as file:
	Log("namespace Offsets {")
	Log("")
	
	for i in range(0, len(FunctionPatterns)):
		className = FunctionPatterns[i][0]
		funcCount = len(FunctionPatterns[i][1])
		
		if className == "":
			className = "General"
		
		Log("    public static class %s {" % className)
		Log("        public static uint")
		
		for j in range(0, funcCount):
			matchedFuncs = []
			pattern      = FunctionPatterns[i][1][j][1]
			currentMatch = 0
			
			while 1:
				currentMatch = FindBinary(currentMatch + 1, SEARCH_DOWN, pattern)
				if currentMatch == BADADDR:
					break
				matchedFuncs.append(GetFuncStartAddress(currentMatch))
			
			matchedFuncsCount  = len(matchedFuncs)
			funcName           = FunctionPatterns[i][1][j][0]
			
			if matchedFuncsCount != 1:
				error = "Error: Pattern for function %s returned %u matches, expected 1." % (GetFullFuncName(i, j), matchedFuncsCount)
				print(error)
				Log("            // %s" % error)
				Log("            %s = 0x00000000" % funcName, False)
				if j < funcCount - 1:
					Log(",\n")
				else:
					Log(";")
				continue
			
			funcAddress = matchedFuncs[0]
			
			print("%s: 0x%08X" % (GetFullFuncName(i, j), funcAddress))
			Log("            // %s" % pattern)
			Log("            %s = 0x%08X" % (funcName, funcAddress), False)
			if j < funcCount - 1:
				Log(",\n")
			else:
				Log(";")
			
			currentFuncName = GetFunctionName(funcAddress)
			if currentFuncName == "sub_%X" % funcAddress:
				MakeName(funcAddress, GetFullFuncName(i, j))
			elif currentFuncName != GetFullFuncName(i, j):
				print("Warning: Skipping function rename because it is already named '%s'" % currentFuncName)
			
		Log("    }")
		Log("")
	Log("}", False)
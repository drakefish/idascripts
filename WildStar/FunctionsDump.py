# Name: FunctionsDump.py
# Author: DrakeFish
# Description: Dumps and names some game functions via various exposed information.
#

print "WildStar Functions Dumper started"

# Method 1: Some function seems to be used for basic debug logs. The strings sent to it start with the caller function name.

# sub_4BB800 (WildStarBeta_0.4.7.6072)
PATTERN_LOGFUNC = "C7 ? 08 00 00 00 00 C7 ? 30 03 00 00 00 00 00 00 C7 ? 40 03 00 00 00 00 00 00"

def RenameFunc(ea, str):
	"Renames a function with a name that could already be taken. Will append a number to it if taken."
	newstr = str
	for x in xrange(50):
		if LocByName(newstr) != BADADDR: # if name is taken
			if LocByName(newstr) == ea: # and it's our function
				break # if its our function it already has a good name
		else: #if name is not taken, take it
			MakeName(ea, newstr)
			break
		
		newstr = "%s%d"%(str, x)

logfunc = PrevFunction(FindBinary(0, SEARCH_DOWN, PATTERN_LOGFUNC))
if logfunc != BADADDR:
	with open("funcsdump.txt", "w") as file:
		call = RfirstB(logfunc)
		while call != BADADDR:
			op = PrevHead(call, 0)
			for x in xrange(5):
				if GetMnem(op) == "push":
					str = GetString(GetOperandValue(op, 0))
					str = str.split(" ")[0]
					file.write("%8X %s\n"%(PrevFunction(op), str))
					RenameFunc(PrevFunction(op), str)
					break
				op = PrevHead(op, 0)
			
			call = RnextB(logfunc, call)

print "done."
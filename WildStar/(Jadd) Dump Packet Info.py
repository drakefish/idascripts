PATTERN_REGISTER_HANDLERS = "55 8B EC 51 56 8B 35 ? ? ? ? C7 05 ? ? ? ? ? ? ? ? C7 05"

def Log(msg, newLine = True):
	print(msg)
	file.write(msg)
	if newLine:
		file.write("\n")

with open("PacketHandlers.txt", "w") as file:
	functionCount = 0
	registerFunction = 0
	packetCount = 0
	
	while 1:
		registerFunction = FindBinary(0 if functionCount == 0 else registerFunction + 1, SEARCH_DOWN, PATTERN_REGISTER_HANDLERS)
		if registerFunction == BADADDR:
			break
		
		functionCount += 1
		functionName = "Network_InitPackets_%u" % functionCount
		Log("Found %s (0x%08X):" % (functionName, registerFunction))
		
		funcEnd = FindFuncEnd(registerFunction)
		if funcEnd == BADADDR:
			Log("The function end address could not be found!")
			continue
		
		currentFuncName = GetFunctionName(registerFunction)
		if currentFuncName == "sub_%X" % registerFunction:
			MakeName(registerFunction, functionName)
		elif currentFuncName != functionName:
			print("Warning: Skipping function rename because it is already named '%s'" % currentFuncName)
		
		# First packet info is after the call
		head = registerFunction
		while head < funcEnd:
			head = NextHead(head, BADADDR)
			if GetMnem(head) == "call":
				break
				
		if head == funcEnd:
			Log("Hit the end of the function early!")
			continue
		
		packets = []
		
		while 1:
			func1 = 0
			func2 = 0
			func3 = 0
			size = 0
			opcode = 0
			
			# Get info from the 5 pushes
			for i in xrange(0, 5):
				while head < funcEnd:
					head = NextHead(head, BADADDR)
					if GetMnem(head) == "push":
						break
				
				if head == funcEnd:
					break
				
				if i == 0:
					func3 = GetOperandValue(head, 0)
				elif i == 1:
					func2 = GetOperandValue(head, 0)
				elif i == 2:
					func1 = GetOperandValue(head, 0)
				elif i == 3:
					size = GetOperandValue(head, 0)
				elif i == 4:
					opcode = GetOperandValue(head, 0)
			
			if head == funcEnd:
				break
				
			packets.append([opcode, size, func1, func2, func3])
			packetCount += 1
		
		packets.sort()
		
		for i in range(0, len(packets)):
			Log("  Packet #%04X (0x%04X) [0x%08X, 0x%08X, 0x%08X]" % (packets[i][0], packets[i][1], packets[i][2], packets[i][3], packets[i][4]))
		
		Log("")
	Log("Done. Total function count: %u - Total packet count: %u" % (functionCount, packetCount), False)
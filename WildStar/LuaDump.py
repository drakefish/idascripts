# Name: LuaDump.py
# Author: DrakeFish
# Description: Dumps and names the lua functions and methods for lua libs. Doesn't include WindowControls
# Status: Complete

import re

# note: We could add discovery of classes that don't have any lib (i.e. Vector3)

# important note: These patterns and scripts will work with versions 0.5+
#  it seems like they have different compiler options compared to 0.4
#  if they change these options again at another point, we will need to make different kinds of patterns 

PATTERN_LUA_ADDLIB = "74 ? 68 ? ? ? ? BA EE D8 FF FF E8 ? ? ? ? 8B 4E 08 83 C4 04 83 CA FF 68 ? ? ? ? E8"
PATTERN_LUA_CREATETYPE = "55 8B EC 83 E4 F8 83 EC 14 ? ? ? 8B ? 8B ? 8B ? 10"
PATTERN_LUA_ADDMETATABLE = "55 8B EC 83 E4 F8 83 EC 1C 53 56 8B C2 57 8B 7D 08"

func_luaaddlib = PrevFunction(FindBinary(0, SEARCH_DOWN, PATTERN_LUA_ADDLIB))
func_luacreatetype = FindBinary(0, SEARCH_DOWN, PATTERN_LUA_CREATETYPE)
func_luaaddmetatable = FindBinary(0, SEARCH_DOWN, PATTERN_LUA_ADDMETATABLE)
r = re.compile('_(.*?)\]')

fcount = 0
ccount = 0

if func_luaaddlib == BADADDR:
	quit("Failed to find LuaAddLib function")
elif func_luacreatetype == BADADDR:
	quit("Failed to find LuaCreateType function")
elif func_luaaddmetatable == BADADDR:
	quit("Failed to find LuaAddMetatable function")

def GetNextCallInFunc(ea, call):
	"Finds the next call in the function. ea represents the point to start from."
	funcend = FindFuncEnd(ea)
	if funcend == BADADDR:
		return BADADDR
	
	head = NextHead(ea, BADADDR)
	while head < funcend:
		if GetMnem(head) == "call" and GetOperandValue(head, 0) == call:
			return head
		head = NextHead(head, BADADDR)
	return BADADDR
	
def GetArrayFromData(ea):
	retArray = []
	tableitem = ea
	while Dword(tableitem) != 0 and Dword(tableitem+4) != 0:
		str = GetString(Dword(tableitem))
		func = Dword(tableitem+4)
		retArray.append([str, func])
		tableitem += 8
	return retArray
	
def GetPushedArray(instr):
	"Returns an array of function defs."
	retArray = []
	
	if GetMnem(instr) != "push":
		print "returning []"
		return []
	
	if GetOpType(instr, 0) == o_imm:
		# data ref, simple .data table
		return GetArrayFromData(GetOperandValue(instr, 0))
	elif GetOpType(instr, 0) == o_reg:
		lea = PrevHead(instr, 0)
		if GetOperandValue(instr, 0) == GetOperandValue(lea, 0):
			var = r.search(GetOpnd(lea, 1)).group(1)
			
			head = PrevHead(lea, 0)
			while head != PrevFunction(instr):
				if GetDisasm(head) == "rep movsd":
					# possibly a memcopy to stack
					head = PrevHead(head, 0)
					m = r.search(GetOpnd(lea, 1))
					if GetOperandValue(head, 0) == 7 and m and m.group(1) == var:
						head = PrevHead(head, 0)
						# if esi and data
						if GetOperandValue(head, 0) == 6 and GetOpType(head, 1) == o_imm:
							return GetArrayFromData(GetOperandValue(head, 1))
							
				elif GetMnem(head) == "mov" and GetOpType(head, 0) == o_displ and GetOpType(head, 1) == o_imm:
					m = r.search(GetOpnd(head, 1))
					if m and m.group(1) == var:
						# inline writing to the stack
						while True:
							strptr = GetOperandValue(head, 1)
							head = NextHead(head, BADADDR)
							func = GetOperandValue(head, 1)
							head = NextHead(head, BADADDR)
							if strptr == 0:
								break # stop at the last write to stack, which is [0, 0]
							retArray.append([GetString(strptr), func])
						return retArray
						
				head = PrevHead(head, 0)
				
	return retArray
		
with open("luadump.txt", "w") as file:
	call = RfirstB(func_luaaddlib)
	while call != BADADDR:
		print "%X"%call
		libstr = None
		libfunc = None
		head = call
		for x in xrange(8):
			head = PrevHead(head, 0)
			if GetMnem(head) == "push" and GetOpType(head, 0) == o_imm:
				libstr = GetString(GetOperandValue(head, 0))
				break
		if not libstr:
			print "Call %X didn't match required pattern. (2)"%call
			call = RnextB(func_luaaddlib, call)
			continue
		for x in xrange(3):
			head = PrevHead(head, 0)
			if GetMnem(head) == "push" and GetOpType(head, 0) == o_imm:
				libfunc = GetOperandValue(head, 0)
				break
		if not libfunc:
			print "Call %X didn't match required pattern. (1)"%call
			call = RnextB(func_luaaddlib, call)
			continue
		
		file.write("\n%s: (%X)\n"%(libstr, libfunc))
		
		# search for classes
		lastaddmetacall = libfunc
		classcall = GetNextCallInFunc(libfunc, func_luacreatetype)
		while classcall != BADADDR:
			classname = ""
			head = PrevHead(PrevHead(classcall, 0), 0)
			funcstart = PrevFunction(classcall)
			while True:
				if GetMnem(head) == "mov" and GetOperandValue(head, 0) == 2: # mov edx, x
					classname = GetString(GetOperandValue(head, 1))
					file.write("\t[class: %s]\n"%classname)
					break
				if head == funcstart:
					quit("failed to find the name of class for classcall %X"%classcall)
				head = PrevHead(head, 0)
				
			# parse this class's content. Find next AddMetatable call
			addmetacall = GetNextCallInFunc(classcall, func_luaaddmetatable)
			head = addmetacall
			for i in xrange(3):
				head = PrevHead(head, 0)
				if GetMnem(head) == "push":
					items = GetPushedArray(head)
					for item in items:
						file.write("\t\t%08X %s\n"%(item[1], item[0]))
						MakeName(item[1], "Script_%s:%s"%(classname, item[0]))
						fcount += 1
					break
			
			lastaddmetacall = addmetacall
			ccount += 1
			classcall = GetNextCallInFunc(classcall, func_luacreatetype)
		
		# get "static" funcs for this library
		addmetacall = GetNextCallInFunc(lastaddmetacall, func_luaaddmetatable)
		head = addmetacall
		for i in xrange(3):
			head = PrevHead(head, 0)
			if GetMnem(head) == "push":
				items = GetPushedArray(head)
				file.write("\t[static functions]\n")
				for item in items:
					file.write("\t%08X %s\n"%(item[1], item[0]))
					MakeName(item[1], "Script_%s.%s"%(libstr, item[0]))
					fcount += 1
				break
		
		call = RnextB(func_luaaddlib, call)
	file.write("\nClass Count: %d Func Count: %d"%(ccount, fcount))
	
print "Done. Class Count: %d Func Count: %d"%(ccount, fcount)
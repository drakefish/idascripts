#
#
#
#

import os
import os.path

FLAG_WRITABLE = 1 # GetSize() and Write()
FLAG_READABLE = 2 # Read()

FILE_OPCODES = os.path.dirname(os.path.realpath(__file__)) + "\\opcodes"
FILE_OPCODESTEMP = os.path.dirname(os.path.realpath(__file__)) + "\\opcodes_tmp"

class Packet:
	def __init__(self, name, opcode, size, flags):
		self.name = name
		self.opcode = opcode
		self.size = size
		self.flags = flags
	
def GetLastPackets():
	packets = []
	if not os.path.isfile(FILE_OPCODES):
		return packets
	
	with open(FILE_OPCODES) as file:
		line = file.readline()
		while line != "": #eof
			split = line.split(":")
			if len(split) != 4:
				exit("ERROR: Line split count invalid: %s (%d)"%(line, len(split)))
			packets.append(Packet(split[3].rstrip("\n"), int(split[0]), int(split[1]), int(split[2])))
			line = file.readline()
	return packets

def DumpToCSharp(packets):
	with open("Opcode.cs", "w") as file:
		file.write("namespace PacketNinjaLib.Game{\n\tpublic enum Opcode\n\t{\n")
		for packet in packets:
			if packet.name == None or packet.name == "":
				continue
			file.write("\t\t%s = %d,\n"%(packet.name, packet.opcode))
		file.write("\t}\n}\n")
	
DumpToCSharp(GetLastPackets())
print("done.")
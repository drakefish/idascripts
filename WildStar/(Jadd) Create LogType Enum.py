PATTERN_SENDTYPEPACKET = "55 8B EC 83 E4 F8 83 EC 18 56 8B F1 57 83 BE"

sendFunction = FindBinary(0, SEARCH_DOWN, PATTERN_SENDTYPEPACKET)
if sendFunction == BADADDR:
	quit("Could not find CGame::SendTypeXPacket functions!")

def Log(msg, newLine = True):
	print(msg)
	file.write(msg)
	if newLine:
		file.write("\n")

with open("LogTypeEnums.txt", "w") as file:
	# Find the "push 5", then the following call
	head = sendFunction
	funcEnd = FindFuncEnd(sendFunction)
	
	if funcEnd == BADADDR:
		quit("Could not find the end of CGame::SendTypeXPacket function!")
	
	while head < funcEnd:
		head = NextHead(head, BADADDR)
		if GetMnem(head) == "push" and GetOperandValue(head, 0) == 5:
			break
			
	while head < funcEnd:
		head = NextHead(head, BADADDR)
		if GetMnem(head) == "call":
			break
	
	head = GetOperandValue(head, 0)
	funcEnd = FindFuncEnd(head)
	
	if funcEnd == BADADDR:
		quit("Could not find the end of log function!")
	
	# Find the "out of bounds" comparison (cmp esi, XXh)
	while head < funcEnd:
		head = NextHead(head, BADADDR)
		if GetMnem(head) == "cmp" and GetOperandValue(head, 0) == 6: # esi == 6
			break
	
	# Now we got our array length!
	arrayLength = GetOperandValue(head, 1)
	
	# Find the array (mov eax, off_XXXXXXXX[esi*4])
	while head < funcEnd:
		head = NextHead(head, BADADDR)
		if GetMnem(head) == "mov" and GetOperandValue(head, 0) == 0: # eax == 0
			break
	
	head = GetOperandValue(head, 1)
	enumId = AddEnum(GetEnumQty(), "LogType", 0)
	
	Log("public enum LogType {")
	
	for i in range(0, arrayLength):
		valName = GetString(Dword(head + (i*4)), -1, ASCSTR_UNICODE)
		Log("\t%s = %u," % (valName, i))
		AddConstEx(enumId, "LogType_%s" % valName, i, -1)
		
	Log("}", False)
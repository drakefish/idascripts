#
#
#
#

import os
import os.path
from helpers import find_head_next
from helpers import find_head_prev
from difflib import SequenceMatcher

FLAG_WRITABLE = 1 # GetSize() and Write()
FLAG_READABLE = 2 # Read()

Network_InitPackets = [ [], "55 8B EC 51 56 8B 35 ? ? ? ? C7 05 ? ? ? ? ? ? ? ? C7 05 ? ? ? ? ? ? 00 00" ]

FILE_OPCODES = os.path.dirname(os.path.realpath(__file__)) + "\\opcodes"
FILE_OPCODESTEMP = os.path.dirname(os.path.realpath(__file__)) + "\\opcodes_tmp"


class Packet:
	def __init__(self, name, opcode, size, flags):
		self.name = name
		self.opcode = opcode
		self.size = size
		self.flags = flags
	def CompareFields(self, other):
		return self.opcode == other.opcode and self.size == other.size and self.flags == other.flags
	# NOTE: The following are used for diff, they are not relevant for other uses. (missing fields in equality and hash)
	def __eq__(self, other):
		return self.size == other.size and self.flags == other.flags
	def __ne__(self, other):
		return self.__eq__(other)
	def __hash__(self): # same as for __eq__
		return hash((self.size, self.flags))
	
def InitPatterns():
	ea = FindBinary(0, SEARCH_DOWN, Network_InitPackets[1])
	if ea == BADADDR: exit("Can't find Network_InitPackets function.")
	while ea != BADADDR:
		Network_InitPackets[0].append(ea)
		ea = FindBinary(ea+1, SEARCH_DOWN, Network_InitPackets[1])

def GetLastPackets():
	packets = []
	if not os.path.isfile(FILE_OPCODES):
		return packets
	
	with open(FILE_OPCODES) as file:
		line = file.readline()
		while line != "": #eof
			split = line.split(":")
			if len(split) != 4:
				exit("ERROR: Line split count invalid: %s (%d)"%(line, len(split)))
			packets.append(Packet(split[3].rstrip("\n"), int(split[0]), int(split[1]), int(split[2])))
			line = file.readline()
	return packets
		
def GetPackets():
	packets = []
	for func in Network_InitPackets[0]:
		_call = find_head_next(func, "call", "dword ptr [eax]", 20)
		while _call != BADADDR:
			pushes = []
			_push =  _call
			for x in xrange(5):
				_push = find_head_prev(_push, "push")
				pushes.append(GetOperandValue(_push, 0))
			flags = 0
			if pushes[3] != 0 : flags |= FLAG_WRITABLE
			if pushes[4] != 0 : flags |= FLAG_READABLE

			packets.append(Packet("", pushes[0], pushes[1], flags))
			
			_call = find_head_next(_call, "call", None, 10)
	print("found %d packets."%len(packets))
	return sorted(packets, key=lambda pkt: pkt.opcode) 
	
def ProcessPackets(last, curr):
	# if there was no previous entry, just make a fresh list.
	if len(last) == 0:
		print("First run, creating a fresh opcodes file.")
		return curr
	
	packets = []
	# no new packets, only needs packets update
	#if len(last) == len(curr):
	#	print("Last and current length are the same.")
	#	for x in xrange(len(curr)):
	#		packets.append(Packet(last[x].name, curr[x].opcode, curr[x].size, curr[x].flags))
	#	return packets
	
	replaced = False
	matcher = SequenceMatcher(a=last, b=curr)
	print("ratio %f"%matcher.ratio())
	opcodes = matcher.get_opcodes()
	for tag, i1, i2, j1, j2 in opcodes:
		print ("%7s a[%d:%d] b[%d:%d]" % (tag, i1, i2, j1, j2))
	for tag, i1, i2, j1, j2 in opcodes:
		if tag == "equal" or tag == "replace":
			if j2 - j1 != i2 - i1:
				exit("ERROR: equal or replace with different lengths, names would be lost.")
			for x in xrange(j2-j1):
				curr[j1+x].name = last[i1+x].name
		
		if tag == "equal":
			packets.extend(curr[j1:j2])
		elif tag == "insert":
			packets.extend(curr[j1:j2])
		elif tag == "replace":
			packets.extend(curr[j1:j2])
			replaced = True
		elif tag == "delete":
			for x in xrange(i2 - i1):
				print("removed packet at %d"%(j1+x))
	
	if replaced == True:
		print("Note: Some packets were replaced, the renaming may have been wrong.")
	
	return packets
	
def WritePackets(packets):
	if os.path.isfile(FILE_OPCODES):
		os.remove(FILE_OPCODES)
	with open(FILE_OPCODES, "w") as file:
		for pkt in packets:
			if pkt.name == None:
				pkt.name = ""
			file.write("%d:%d:%d:%s\n"%(pkt.opcode, pkt.size, pkt.flags, pkt.name))
	
InitPatterns()
packets = ProcessPackets(GetLastPackets(), GetPackets())
WritePackets(packets)
print("done")
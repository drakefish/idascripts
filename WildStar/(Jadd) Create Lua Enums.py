import idaapi
import idautils
import idc


lua_registerEnum = FindBinary(0, SEARCH_DOWN, "55 8B EC 8B 45 08 53 56 57 8B F9 85 C0 75 0C")


if lua_registerEnum == BADADDR:
    print "label lua funcs: Failed to locate lua_registerEnum"
else:    
    ea = lua_registerEnum
    
    xRef = idc.RfirstB( lua_registerEnum )
    
    f = open("Enums.cs", "w")
    
    numEnums = 0
    
    while 1:
        head = idc.PrevHead(xRef, 8)
        
        # mov ecx, edi
        if idc.GetMnem(head) == "mov":
            head = idc.PrevHead(head, 8)
        
        # push offset <enum name> 
        # head = idc.PrevHead(head, 8)
        
        name = idc.GetString(idc.GetOperandValue(head, 0), -1, ASCSTR_C)
        
        print("Found Lua enum: %s" % name)
        
        # push offset <enum value table>
        head = idc.PrevHead(head, 8)
        valuesTable = idc.GetOperandValue(head, 0)
        
        # And finally... push <count>
        head = idc.PrevHead(head, 8)
        count = idc.GetOperandValue(head, 0)
        
        f.write( "public enum %s {" % name )
        enumId = AddEnum(GetEnumQty(), name, 0)
        
        offset = 0
        for i in range(count):
            valName = idc.GetString(idc.Dword(valuesTable + offset), -1, ASCSTR_UNICODE)
            # move to the value
            offset+=4
            valValue = idc.Dword(valuesTable + offset)
            f.write( "\n    %s = %d," % (valName, valValue) )
            AddConstEx(enumId, "%s_%s" % (name, valName), valValue, -1)
            # move to the next string
            offset+=4
        
        f.write( "\n}" )
        
        numEnums += 1
        
        # Yeah... just set the next xref
        xRef = idc.RnextB( lua_registerEnum, xRef )
        
        if xRef == BADADDR:
            break
        
        f.write( "\n\n" )
	
    f.close()
#
# These are all general helper functions my scripts may need.
#

from idc import *

def __find_head(head, mnem, opnd, maxiters, inclusive, fGetHead, maxHead):
	h=head
	start = 0
	if inclusive and GetMnem(h) == mnem:
		start = 1
		if opnd != None:
			if GetOpnd(h, 0) == opnd:
				return h
		else:
			return h
	for x in range(start, maxiters):
		h = fGetHead(h, maxHead)
		if GetMnem(h) == mnem:
			if opnd != None:
				if GetOpnd(h, 0) == opnd:
					return h
			else:
				return h
	return BADADDR

def find_head_prev(head, mnem, opnd=None, maxiters=5, inclusive = False):
	return __find_head(head, mnem, opnd, maxiters, inclusive, PrevHead, 0)	

def find_head_next(head, mnem, opnd=None, maxiters=5, inclusive = False):
	return __find_head(head, mnem, opnd, maxiters, inclusive, NextHead, BADADDR)
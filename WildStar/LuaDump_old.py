#
# Author: DrakeFish
#
#

from sys import exit
import re

# todo: Find types declarations via the function
# todo: Get both static and members metatable for it

# important note: These patterns and scripts will work with versions 0.5+
#  it seems like they have different compiler options compared to 0.4
#  if they change these options again at another point, we will need to make different kinds of patterns 

PATTERN_LUA_CREATETYPE = "55 8B EC 83 E4 F8 83 EC 14 ? ? ? 8B ? 8B ? 8B ? 10"
PATTERN_LUA_ADDMETATABLE = "55 8B EC 83 E4 F8 83 EC 1C 53 56 8B C2 57 8B 7D 08"

func_luacreatetype = FindBinary(0, SEARCH_DOWN, PATTERN_LUA_CREATETYPE)
func_luaaddmetatable = FindBinary(0, SEARCH_DOWN, PATTERN_LUA_ADDMETATABLE)
r = re.compile('_(.*?)\]')

fcount = 0
ccount = 0

if func_luacreatetype == BADADDR:
	quit("Failed to find LuaCreateType function")
elif func_luaaddmetatable == BADADDR:
	quit("Failed to find LuaAddMetatable function")

def GetNextCallInFunc(ea, call):
	"Finds the next call in the function. ea represents the point to start from."
	funcend = FindFuncEnd(ea)
	if funcend == BADADDR:
		return BADADDR
	
	head = NextHead(ea, BADADDR)
	while head < funcend:
		if GetMnem(head) == "call" and GetOperandValue(head, 0) == call:
			return head
		head = NextHead(head, BADADDR)
	return BADADDR
	
def GetArrayFromData(ea):
	retArray = []
	tableitem = ea
	while Dword(tableitem) != 0 and Dword(tableitem+4) != 0:
		str = GetString(Dword(tableitem))
		func = Dword(tableitem+4)
		retArray.append([str, func])
		tableitem += 8
	return retArray
	
def GetPushedArray(instr):
	"Returns an array of function defs."
	# note: There are at least 3 different types of listing.
	# #todo1: Pushes a pointer to the array in .data (see 0x0)
	# todo2: Copies the array to the stack via rep movsd (see 0x00470164) (lea eax, [ptr]  push eax)
	# todo3: Inlined code puts the array on the stack via a repetitive pattern (see 0x004963A3) (lea eax, [ptr]  push eax)
	retArray = []
	
	if GetMnem(instr) != "push":
		print "returning []"
		return []
	
	if GetOpType(instr, 0) == o_imm:
		# data ref, simple .data table
		return GetArrayFromData(GetOperandValue(instr, 0))
	elif GetOpType(instr, 0) == o_reg:
		lea = PrevHead(instr, 0)
		if GetOperandValue(instr, 0) == GetOperandValue(lea, 0):
			var = r.search(GetOpnd(lea, 1)).group(1)
			
			head = PrevHead(lea, 0)
			while head != PrevFunction(instr):
				if GetDisasm(head) == "rep movsd":
					# possibly a memcopy to stack
					head = PrevHead(head, 0)
					m = r.search(GetOpnd(lea, 1))
					if GetOperandValue(head, 0) == 7 and m and m.group(1) == var:
						head = PrevHead(head, 0)
						# if esi and data
						if GetOperandValue(head, 0) == 6 and GetOpType(head, 1) == o_imm:
							return GetArrayFromData(GetOperandValue(head, 1))
							
				elif GetMnem(head) == "mov" and GetOpType(head, 0) == o_displ and GetOpType(head, 1) == o_imm:
					m = r.search(GetOpnd(head, 1))
					if m and m.group(1) == var:
						# inline writing to the stack
						while True:
							strptr = GetOperandValue(head, 1)
							head = NextHead(head, BADADDR)
							func = GetOperandValue(head, 1)
							head = NextHead(head, BADADDR)
							if strptr == 0:
								break # stop at the last write to stack, which is [0, 0]
							retArray.append([GetString(strptr), func])
						return retArray
						
				head = PrevHead(head, 0)
				
	return retArray
		
print "\n\n\n-----------------------------------------------------"
with open("luadump.txt", "w") as file:
	file.write("Note: A present but empty \"members\" or \"static\" means that its parser is bugged/unsupported.\n")
	call = RfirstB(func_luacreatetype)
	while call != BADADDR:
		head = PrevHead(PrevHead(call, 0), 0)
		funcstart = PrevFunction(call)
		while True:
			if GetMnem(head) == "mov" and GetOperandValue(head, 0) == 2: # mov edx, x
				break
			if head == funcstart:
				quit("failed to find the name of class for call %X"%call)
			head = PrevHead(head, 0)
				
		str = GetString(GetOperandValue(head, 1))
		if str != "":
			file.write("\n%s:\n"%str)
			addcall = call
			for x in xrange(2):
				addcall = GetNextCallInFunc(addcall, func_luaaddmetatable)
				if addcall == BADADDR:
					#print "\"missing\" a call"
					break
				
				head = addcall
				for i in xrange(3):
					head = PrevHead(head, 0)
					if GetMnem(head) == "push":
						# it's the push
						pushtable = head
						items = GetPushedArray(pushtable)
					elif GetOperandValue(head, 0) == 2: # edx
						# it's the edx value
						if GetMnem(head) == "xor": file.write(" members:\n")
						else: file.write(" static:\n")
				
				#print items
				for item in items:
					file.write("\t%8X %s\n"%(item[1], item[0]))
					fcount += 1
				ccount += 1
		else:
			print ("error: 2nd op isnt string: %8X"%head)
			
		call = RnextB(func_luacreatetype, call)
		#print call
	file.write("\nClasses Count: %d Functions Count: %d"%(ccount, fcount))

	# TODO: There is another type of lua declaring that we need to do: func  sub_460780
	
print "Done. Class Count: %d Func Count: %d"%(ccount, fcount)